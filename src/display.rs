use num_traits::Float;
use std::fmt::{Debug, Display, Formatter, Result as FormatResult};

use crate::Vec3;

impl<T: Debug + Float + Display> Display for Vec3<T> {
    fn fmt(&self, f: &mut Formatter<'_>) -> FormatResult {
        write!(f, "{}, {}, {}", self.x(), self.y(), self.z())
    }
}

impl<T: Debug + Float + Display> Vec3<T> {
    pub fn to_delimited_string(&self, del: Option<char>) -> String {
        let del = del.unwrap_or(' ');
        format!("{}{}{}{}{}", self.x(), del, self.y(), del, self.z())
    }
}

#[cfg(test)]
pub mod test {
    use super::Vec3;

    #[test]
    fn display() {
        let sut = Vec3::<f64>::default();

        let res = sut.to_string();

        assert_eq!(res, "0, 0, 0".to_string())
    }

    #[test]
    fn display_delimited_values() {
        let sut = Vec3::new(1.111, 2.555, 5.999);

        let res = sut.to_delimited_string(None);

        assert_eq!(res, "1.111 2.555 5.999".to_string())
    }
}
